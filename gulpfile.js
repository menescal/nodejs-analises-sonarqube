var gulp = require('gulp');
var sonar = require('gulp-sonar');
var util = require('util');

gulp.task('default', function () {
    var options = {
        sonar: {
            host: {
                url: 'http://localhost:9000'
            },
            projectKey: 'TI',
            projectName: 'TI - Plataformas Digitais',
            projectVersion: '1.0.0',
            sourceEncoding: 'UTF-8',
            projectBaseDir: 'C:/Menescal/Desenvolvimento/node-workspace/repositorios/nodejs-analises-sonarqube',
            sources: 'testeUnitarioEx',
            language: 'js',
            sourceEncoding: 'UTF-8',
            exclusions: 'testeUnitarioEx/coverage/**/*,testeUnitarioEx/node_modules/**/*,testeUnitarioEx/tdd/**/*,testeUnitarioEx/package.json,testeUnitarioEx/sonar.properties.js,testeUnitarioEx/gulpfile.js',
            javascript: {
                lcov: {
                    reportPath: '/coverage/lcov.info'
                }
            }
        }
    };

    return gulp.src('thisFileDoesNotExist.js', { read: false })
        .pipe(sonar(options))
        .on('error', util.log);
});