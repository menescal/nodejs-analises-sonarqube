const test = require('tape')
const index = require('../index.js')

test('Aplicar desconto', (t) => {
    t.assert(index.aplicarDesconto(10,5) === 5, "Descontou corretamente (index.js)")
    t.end()
})